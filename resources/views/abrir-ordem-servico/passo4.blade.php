<html>
<head>
    <title>Criar Chamado - Passo 2</title>
    <link rel="stylesheet" type="text/css" href="../../css/style.css?v={{ filemtime('css/style.css') }}">
    <link>
</head>
<body>
<div class="central">
    <div class="topo">
        <br>
        <br>
        <br>
        <div style="width: 700px; margin: auto">
            <div style="float: left; padding-right: 250px">
                <a href="{{ route('/') }}">
                    <h4>VOLTAR</h4>
                </a>
            </div>
            <div style="float: left;">
                <a href="{{ route('/') }}">
                    <h4>INICIO</h4>
                </a>
            </div>
        </div>
    </div>
    <div class="conteudo">
        <div class="setores">
            <h1>4º - QUAL CATEGORIA?</h1>
            <div class="botoes">
                <form method="post" action="{{ route('abrir-ordem-servico') }}">
                    <input type="hidden" name="setor" value="{{ $setor }}">
                    <input type="hidden" name="tipo" value="{{ $tipo }}">
                    <input type="hidden" name="categoria" value="{{ $categoria }}">
                    <select id="catalogo" name="catalogo">
                        <option>Selecione um Catalogo</option>
                        @foreach($catalogos as $catalogo)
                            <option value="{{ $catalogo->id }}">{{ $catalogo->catalogo }}</option>
                        @endforeach
                    </select>
                    <br>
                    <br>
                    <label for="texto">Descreva sua solicitação</label>
                    <br>
                    <textarea id="texto" name="texto" cols="50" rows="7"></textarea>
                    <br>
                    <br>
                    <input type="submit" value="ABRIR CHAMADO">
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>