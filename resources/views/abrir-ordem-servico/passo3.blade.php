<html>
<head>
    <title>Criar Chamado - Passo 2</title>
    <link rel="stylesheet" type="text/css" href="../../css/style.css?v={{ filemtime('css/style.css') }}">
    <link>
</head>
<body>
<div class="central">
    <div class="topo">
        <br>
        <br>
        <br>
        <div style="width: 700px; margin: auto">
            <div style="float: left; padding-right: 250px">
                <a href="{{ route('/') }}">
                    <h4>VOLTAR</h4>
                </a>
            </div>
            <div style="float: left;">
                <a href="{{ route('/') }}">
                    <h4>INICIO</h4>
                </a>
            </div>
        </div>
    </div>
    <div class="conteudo">
        <div class="setores">
            <h1>3º - QUAL CATEGORIA?</h1>
            <div class="botoes">
                <form method="post" action="{{ route('abrir-ordem-servico-passo-4') }}">
                    <input type="hidden" name="setor" value="{{ $setor }}">
                    <input type="hidden" name="tipo" value="{{ $tipo }}">
                    @foreach($categorias as $categoria)
                        <button name="categoria" value="{{ $categoria->id }}">{{ $categoria->categoria }}</button>
                    @endforeach
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>