<html>
<head>
    <title>Criar Chamado - Passo 1</title>
    <link rel="stylesheet" type="text/css" href="../../css/style.css?v={{ filemtime('css/style.css') }}">
    <link>
</head>
<body>
<div class="central">
    <div class="topo">
        <br>
        <br>
        <br>
        <div style="width: 700px; margin: auto">
            <div style="float: left; padding-right: 250px">
                <a href="{{ route('/') }}">
                    <h4>VOLTAR</h4>
                </a>
            </div>
            <div style="float: left;">
                <a href="{{ route('/') }}">
                    <h4>INICIO</h4>
                </a>
            </div>
        </div>
    </div>
    <div class="conteudo">
        <div class="setores">
            <h1>1º - QUAL O SEU SETOR?</h1>
            (Informe o setor em que você se encontra)
            <div class="botoes">
                <form method="post" action="{{ route('abrir-ordem-servico-passo-2') }}">
                    @foreach($setores as $setor)
                        <button name="setor" value="{{ $setor->id }}">{{ $setor->setor }}</button>
                    @endforeach
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>