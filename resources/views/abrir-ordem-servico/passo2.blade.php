<html>
<head>
    <title>Criar Chamado - Passo 2</title>
    <link rel="stylesheet" type="text/css" href="../../css/style.css?v={{ filemtime('css/style.css') }}">
    <link>
</head>
<body>
<div class="central">
    <div class="topo">
        <br>
        <br>
        <br>
        <div style="width: 700px; margin: auto">
            <div style="float: left; padding-right: 250px">
                <a href="{{ route('/') }}">
                    <h4>VOLTAR</h4>
                </a>
            </div>
            <div style="float: left;">
                <a href="{{ route('/') }}">
                    <h4>INICIO</h4>
                </a>
            </div>
        </div>
    </div>
    <div class="conteudo">
        <div class="setores">
            <h1>2º - QUAL O TIPO DE SOLICITAÇÃO?</h1>
            <div class="botoes">
                <form method="post" action="{{ route('abrir-ordem-servico-passo-3') }}">
                    <input type="hidden" name="setor" value="{{ $setor }}">
                    @foreach($tipos as $tipo)
                        <button name="tipo" value="{{ $tipo->id }}">{{ $tipo->tipo }}</button>
                    @endforeach
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>