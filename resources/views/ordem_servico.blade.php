<html>
<head>
    <title>Chamado {{ $ordem_servico->id }}</title>
    <link rel="stylesheet" type="text/css" href="../css/style.css?v={{ filemtime('css/style.css') }}">
    <link>
</head>
<body>
<div class="central">
    <div class="topo">
        <br>
        <br>
        <br>
        <div style="width: 700px; margin: auto">
            <div style="float: left; padding-right: 250px">
                <a href="{{ route('ordem-servico') }}">
                    <h4>CHAMADOS</h4>
                </a>
            </div>
            <div style="float: left;">
                <a href="{{ route('/') }}">
                    <h4>INICIO</h4>
                </a>
            </div>
        </div>
    </div>
    <div class="conteudo">
        <div class="setores">
            <h1>{{ $ordem_servico->id }}# - {{ $ordem_servico->titulo }}</h1>
            <div class="botoes">
                <table class="tabela-chamado">
                    <thead>
                    <tr>
                        <th>Setor onde houve o problema: </th>
                        <th class="resposta">{{ $ordem_servico['setor'] }}</th>
                    </tr>
                    <tr>
                        <th>Categoria: </th>
                        <th class="resposta">{{ $ordem_servico['categoria'] }}</th>
                    </tr>
                    <tr>
                        <th>Dificuldade/Solicitação: </th>
                        <th class="resposta">{{ $ordem_servico['catalogo'] }}</th>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="mensagem">
                <h3>Mensagem</h3>
                <div>
                    {{ $ordem_servico->mensagem }}
                </div>
            </div>
            <div class="interacoes">
                @if($interacoes)
                    @foreach($interacoes as $interacao)
                        <div class="@if($interacao->atendente) atendente @else usuario @endif">
                            @if($interacao->atendente)
                                <h4>Atendente: {{ $atendentes[$interacao->atendente - 1]->nome }}</h4>
                            @else
                                <h4>Usuário</h4>
                            @endif
                            {{ $interacao->mensagem }}
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="interagir">
                <h3>Interagir com chamado</h3>
                <form action="{{ route('interagir') }}" method="post">
                    <input type="hidden" name="idordemservico" value="{{ $ordem_servico->id }}">
                    <textarea name="resposta" cols="50" rows="5"></textarea>
                    <br>
                    <br>
                    <label for="atendente">Selecione um atendente: </label>
                    <select id="atendente" name="atendente">
                        <option value="0">Sem Atendente</option>
                        @if($atendentes AND isset($atendentes))
                            @foreach($atendentes as $atendente)
                                <option value="{{ $atendente->id }}">{{ $atendente->nome }}</option>
                            @endforeach
                        @endif
                    </select>
                    <br>
                    <br>
                    <input type="submit" value="RESPONDER">
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>