<html>
@include('head')
<body>
<h1>Setor</h1>

<form id="form-setor">
    <label for="nome">Nome:</label>
    <input type="text" name="nome">
    <button type="button" id="criar-setor" data-url="{{ route('criar-setor') }}">Criar setor</button>
</form>


<table id="setores" border="1">
    <tr>
        <td>ID</td>
        <td>NOME</td>
        <td id="remover" data-url="{{ route('deletar-setor') }}">REMOVER</td>
    </tr>
    @foreach($setores as $setor)
        <tr data-id="{{ $setor->id }}">
            <td>{{ $setor->id }}</td>
            <td>{{ $setor->setor }}</td>
            <td>
                <button type="button" class="remover-setor" data-id="{{$setor->id}}">X</button>
            </td>
        </tr>
    @endforeach
</table>
</body>
</html>