<html>
@include('head')
<body>
<h1>Atendentes</h1>

<form id="form-atendente">
    <label for="nome">Nome:</label>
    <input type="text" name="nome">
    <button type="button" id="criar-atendente" data-url="{{ route('criar-atendente') }}">Criar Atendente</button>
</form>


<table id="atendentes" border="1">
    <tr>
        <td>ID</td>
        <td>NOME</td>
        <td id="remover" data-url="{{ route('deletar-atendente') }}">REMOVER</td>
    </tr>
    @foreach($atendentes as $atendente)
        <tr data-id="{{ $atendente->id }}">
            <td>{{ $atendente->id }}</td>
            <td>{{ $atendente->nome }}</td>
            <td>
                <button type="button" class="remover-atendente" data-id="{{$atendente->id}}">X</button>
            </td>
        </tr>
    @endforeach
</table>
</body>
</html>