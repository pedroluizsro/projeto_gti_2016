<html>
@include('head')
<body>
<h1>Catalogo de Serviços</h1>

<form id="form-catalogo">
    <label for="nome">Nome:</label>
    <input type="text" name="catalogo">
    <br>
    <label for="categoria">Categoria</label>
    <select name="categoria" id="categoria">
       <option value="false" selected>Categorias</option>
        @foreach($categorias as $categoria)
            <option value="{{ $categoria->id }}">{{ $categoria->categoria }}</option>
        @endforeach
    </select>
    <br>
    <label for="tipo">Tipo</label>
    <select name="tipo" id="tipo">
        <option value="false" selected>Tipos</option>
        @foreach($tipos as $tipo)
            <option value="{{ $tipo->id }}">{{ $tipo->tipo }}</option>
        @endforeach
    </select>
    <br>
    <button type="button" id="criar-catalogo" data-url="{{ route('criar-catalogo') }}">Criar catalogo</button>
</form>


<table id="catalogos" border="1">
    <tr>
        <td>ID</td>
        <td>NOME</td>
        <td>CATEGORIA</td>
        <td>TIPO</td>
        <td id="remover" data-url="{{ route('deletar-catalogo') }}">REMOVER</td>
    </tr>
    @foreach($catalogos as $catalogo)
        <tr data-id="{{ $catalogo['id'] }}">
            <td>{{ $catalogo['id'] }}</td>
            <td>{{ $catalogo['catalogo'] }}</td>
            <td>{{ $catalogo['categoria'] }}</td>
            <td>{{ $catalogo['tipo'] }}</td>
            <td>
                <button type="button" class="remover-catalogo" data-id="{{$catalogo['id']}}">X</button>
            </td>
        </tr>
    @endforeach
</table>
</body>
</html>