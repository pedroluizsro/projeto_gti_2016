<html>
@include('head')
<body>
<h1>Categoria</h1>

<form id="form-categoria">
    <label for="nome">Nome:</label>
    <input type="text" name="categoria">
    <button type="button" id="criar-categoria" data-url="{{ route('criar-categoria') }}">Criar categoria</button>
</form>


<table id="categorias" border="1">
    <tr>
        <td>ID</td>
        <td>NOME</td>
        <td id="remover" data-url="{{ route('deletar-categoria') }}">REMOVER</td>
    </tr>
    @foreach($categorias as $categoria)
        <tr data-id="{{ $categoria->id }}">
            <td>{{ $categoria->id }}</td>
            <td>{{ $categoria->categoria }}</td>
            <td>
                <button type="button" class="remover-categoria" data-id="{{$categoria->id}}">X</button>
            </td>
        </tr>
    @endforeach
</table>
</body>
</html>