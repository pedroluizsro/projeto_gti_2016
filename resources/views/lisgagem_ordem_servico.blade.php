<html>
<head>
    <title>Chamados</title>
    <link rel="stylesheet" type="text/css" href="css/style.css?v={{ filemtime('css/style.css') }}">
    <link>
</head>
<body>
<div class="central">
    <div class="topo">
        <div class="topo">
            <br>
            <br>
            <br>
            <div style="width: 700px; margin: auto">
                <div style="float: left; padding-right: 250px">
                    <a href="{{ route('/') }}">
                        <h4>VOLTAR</h4>
                    </a>
                </div>
                <div style="float: left;">
                    <a href="{{ route('/') }}">
                        <h4>INICIO</h4>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="conteudo">
        <h1>LISTAGEM DE CHAMADOS</h1>
        <div class="cont">
            <table class="tabela">
                <thead>
                <tr>
                    <th>Data</th>
                    <th>Titulo</th>
                    <th>Atendente</th>
                    <th>Categoria</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                @if($ordem_servico AND is_array($ordem_servico))
                    @foreach($ordem_servico as $ordemservico)
                        <tr>
                            <td>{{ date('d-m-Y H:i',strtotime($ordemservico->datahora)) }}</td>
                            <td>
                                <a href="{{ route('ordem-servico-pagina',array('id' => $ordemservico->id)) }}">
                                    {{ $ordemservico->titulo }}
                                </a>
                            </td>
                            <td>@if($ordemservico['atendente']) {{ $ordemservico['atendente'] }} @else Sem Atendente @endif</td>
                            <td>{{ $ordemservico['categoria'] }}</td>
                            <td>{{ $ordemservico['status'] }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">
                            <h3>Não há chamados cadastrados</h3>
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>