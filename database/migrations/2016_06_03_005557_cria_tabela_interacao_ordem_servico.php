<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaTabelaInteracaoOrdemServico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(
            DB::raw(
            '
            BEGIN;

            CREATE TABLE IF NOT EXISTS gti_interacao_ordem_servico (
              id SERIAL PRIMARY KEY,
              mensagem TEXT,
              ordem_servico INTEGER,
              atendente INTEGER,
              datahora TIMESTAMP DEFAULT NOW(),
              CONSTRAINT "gti_interacao_ordem_servico_ordem_servico_fk" FOREIGN KEY ("ordem_servico")
                REFERENCES gti_ordem_servico (id) MATCH SIMPLE
                ON UPDATE CASCADE ON DELETE CASCADE,
              CONSTRAINT "gti_interacao_ordem_servico_atendente_fk" FOREIGN KEY ("atendente")
                REFERENCES gti_atendente (id) MATCH SIMPLE
                ON UPDATE CASCADE ON DELETE CASCADE
            );

            COMMIT;
            '
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interacao_ordem_servico');
    }
}
