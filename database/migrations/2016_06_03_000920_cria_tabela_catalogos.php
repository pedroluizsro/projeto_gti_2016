<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaTabelaCatalogos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(
            DB::raw(
            '
            BEGIN;

            CREATE TABLE IF NOT EXISTS gti_catalogo (
              id SERIAL PRIMARY KEY,
              catalogo VARCHAR(100),
              categoria INTEGER,
              CONSTRAINT "gti_catalogo_categoria_fk" FOREIGN KEY ("categoria")
                REFERENCES gti_categoria(id) MATCH SIMPLE
                ON UPDATE CASCADE ON DELETE CASCADE
            );

            COMMENT ON TABLE gti_catalogo IS \'Catalogo de serviços.\';
            COMMENT ON COLUMN gti_catalogo.id IS \'ID do serviço.\';
            COMMENT ON COLUMN gti_catalogo.catalogo IS \'Serviço em questão\';
            COMMENT ON COLUMN gti_catalogo.categoria IS \'Categoria do Serviço\';

            COMMIT;
            '
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalogo');
    }
}
