<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaTabelaSetores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(
            DB::raw(
                '
                BEGIN;
                
                CREATE TABLE IF NOT EXISTS gti_setor (
                  id SERIAL PRIMARY KEY,
                  setor VARCHAR(100)
                );
                
                COMMENT ON TABLE gti_setor IS \'Tabela de setores\';
                COMMENT ON COLUMN gti_setor.id IS \'ID do Setor\';
                COMMENT ON COLUMN gti_setor.setor IS \'Nome do setor\';

                COMMIT;
                '
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setor');
    }
}
