<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaTabelaOrdemServico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(
            DB::raw(
            '
            BEGIN;

            CREATE TABLE IF NOT EXISTS gti_ordem_servico (
              id SERIAL PRIMARY KEY,
              titulo VARCHAR(100),
              mensagem TEXT,
              setor INTEGER,
              catalogo INTEGER,
              datahora TIMESTAMP DEFAULT NOW(),
              CONSTRAINT "gti_ordem_servico_setor_fk" FOREIGN KEY ("setor")
                REFERENCES gti_setor (id) MATCH SIMPLE
                ON UPDATE CASCADE ON DELETE CASCADE,
              CONSTRAINT "gti_ordem_servico_catalogo_fk" FOREIGN KEY ("catalogo")
                REFERENCES gti_catalogo (id) MATCH SIMPLE
                ON UPDATE CASCADE ON DELETE CASCADE
            );

            COMMENT ON TABLE gti_ordem_servico IS \'Chamados\';
            COMMENT ON COLUMN gti_ordem_servico.id IS \'ID do Chamado\';
            COMMENT ON COLUMN gti_ordem_servico.titulo IS \'Titulo do chamado\';
            COMMENT ON COLUMN gti_ordem_servico.mensagem IS \'Mensagem do chamado\';
            COMMENT ON COLUMN gti_ordem_servico.setor IS \'Setor responsável pelo chamado\';
            COMMENT ON COLUMN gti_ordem_servico.catalogo IS \'Serviço requisitado\';

            COMMIT;
            '
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ordem_servico');
    }
}
