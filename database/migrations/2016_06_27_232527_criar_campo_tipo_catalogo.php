<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarCampoTipoCatalogo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(
            DB::raw(
                '
            BEGIN;

            ALTER TABLE gti_catalogo
              ADD COLUMN tipo INTEGER,
              ADD CONSTRAINT "gti_tipo_catalogo_fk" FOREIGN KEY ("tipo")
            REFERENCES gti_tipo (id) MATCH SIMPLE
            ON UPDATE CASCADE ON DELETE CASCADE;

            COMMIT;
            '
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::unprepared(
            DB::raw(
                '
            BEGIN;

            ALTER TABLE gti_catalogo
            DROP COLUMN tipo,
            DROP CONSTRAINT "gti_tipo_catalogo_fk";

            COMMIT;
            '
            )
        );

    }
}
