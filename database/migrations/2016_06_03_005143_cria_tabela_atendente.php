<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaTabelaAtendente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(
            DB::raw(
            '
            BEGIN;

            CREATE TABLE IF NOT EXISTS gti_atendente (
              id SERIAL PRIMARY KEY,
              nome VARCHAR(100)
            );

            COMMIT;
            '
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atendente');
    }
}
