<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaTabelaCategorias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(
            DB::raw(
            '
            BEGIN;

            CREATE TABLE IF NOT EXISTS gti_categoria (
              id SERIAL PRIMARY KEY,
              categoria VARCHAR(100)
            );

            COMMENT ON TABLE gti_categoria IS \'Categorias de serviços\';
            COMMENT ON COLUMN gti_categoria.id IS \'ID da categoria\';
            COMMENT ON COLUMN gti_categoria.categoria IS \'Nome da categoria\';

            COMMIT;
            '
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categoria');
    }
}
