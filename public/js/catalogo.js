/**
 * Created by pedro on 16/06/16.
 */
$(document).ready( function () {

    $('#criar-catalogo').on('click',function () {
        var botao = $(this);
        var nome = $('#form-catalogo input[name=catalogo]').val();
        var idcategoria = $('#categoria option:selected').val();
        var idtipo = $('#tipo option:selected').val();
        if(idcategoria == "false"){
            alert("Selecione uma categoria");
            return false;
        }
        if(idtipo == "false"){
            alert("Selecione um tipo");
            return false;
        }
        $.ajax({
            url: botao.data('url'),
            method: 'post',
            data: {
                catalogo: nome,
                idcategoria: idcategoria,
                idtipo: idtipo
            },
            dataType: 'json'
        }).done(function(retorno) {

            if(retorno.resultado == 'ok'){
                var tr = '';
                tr += '<td>'+retorno.dados.id+'</td>';
                tr += '<td>'+retorno.dados.catalogo+'</td>';
                tr += '<td>'+retorno.dados.categoria+'</td>';
                tr += '<td>'+retorno.dados.tipo+'</td>';
                tr += '<td><button type="button" class="remover-catalogo" data-id="'+retorno.dados.id+'">X</button></td>';
                $('#catalogos').append('<tr data-id="'+retorno.dados.id+'">'+tr+'</tr>');
                $('#form-catalogo input[name=catalogo]').val('');
            } else if (retorno.motivo == 'nao-preenchido') {
                alert('Preencha os campos corretamente');
            } else if (retorno.motivo == 'nao-salvou'){
                alert('Falha ao criar');
            }

        });
    });

    $('#catalogos').on('click','.remover-catalogo',function () {
        var idcatalogo = $(this).data('id');
        var url = $('#remover').data('url');
        var tr = $('tr[data-id="'+idcatalogo+'"]');
        $.ajax({
            url: url,
            method: 'post',
            data: {
                id: idcatalogo
            },
            dataType: 'json'
        }).done(function(retorno) {

            if(retorno.resultado == 'ok'){
                tr.remove();
            } else if (retorno.motivo == 'invalido'){
                alert('Usuário não existe');
            } else if (retorno.motivo == 'nao-deu'){
                alert('Falha ao deletar');
            }

        });


    });

});