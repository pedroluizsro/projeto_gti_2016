/**
 * Created by pedro on 09/06/16.
 */
$(document).ready( function () {
    
    $('#criar-atendente').on('click',function () {
        var botao = $(this);
        var nome = $('#form-atendente input[name=nome]').val();
        $.ajax({
            url: botao.data('url'),
            method: 'post',
            data: {
                nome: nome
            },
            dataType: 'json'
        }).done(function(retorno) {

            if(retorno.resultado == 'ok'){
                var tr = '';
                tr += '<td>'+retorno.dados.id+'</td>';
                tr += '<td>'+retorno.dados.nome+'</td>';
                tr += '<td><button type="button" class="remover-atendente" data-id="'+retorno.dados.id+'">X</button></td>';
                $('#atendentes').append('<tr data-id="'+retorno.dados.id+'">'+tr+'</tr>');
                $('#form-atendente input[name=nome]').val('');
            } else if (retorno.motivo == 'nao-preenchido') {
                alert('Preencha os campos corretamente');
            } else if (retorno.motivo == 'nao-salvou'){
                alert('Falha ao criar');
            }

        });
    });

    $('#atendentes').on('click','.remover-atendente',function () {
        var idatendente = $(this).data('id');
        var url = $('#remover').data('url');
        var tr = $('tr[data-id="'+idatendente+'"]');
        $.ajax({
            url: url,
            method: 'post',
            data: {
                id: idatendente
            },
            dataType: 'json'
        }).done(function(retorno) {

            if(retorno.resultado == 'ok'){
                tr.remove();
            } else if (retorno.motivo == 'invalido'){
                alert('Usuário não existe');
            } else if (retorno.motivo == 'nao-deu'){
                alert('Falha ao deletar');
            }

        });


    });
    
});