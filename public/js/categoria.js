/**
 * Created by pedro on 09/06/16.
 */
$(document).ready( function () {

    $('#criar-categoria').on('click',function () {
        var botao = $(this);
        var categoria = $('#form-categoria input[name=categoria]').val();
        $.ajax({
            url: botao.data('url'),
            method: 'post',
            data: {
                categoria: categoria
            },
            dataType: 'json'
        }).done(function(retorno) {

            if(retorno.resultado == 'ok'){
                var tr = '';
                tr += '<td>'+retorno.dados.id+'</td>';
                tr += '<td>'+retorno.dados.categoria+'</td>';
                tr += '<td><button type="button" class="remover-categoria" data-id="'+retorno.dados.id+'">X</button></td>';
                $('#categorias').append('<tr data-id="'+retorno.dados.id+'">'+tr+'</tr>');
                $('#form-categoria input[name=categoria]').val('');
            } else if (retorno.motivo == 'nao-preenchido') {
                alert('Preencha os campos corretamente');
            } else if (retorno.motivo == 'nao-salvou'){
                alert('Falha ao criar');
            }

        });
    });

    $('#categorias').on('click','.remover-categoria',function () {
        var idcategoria = $(this).data('id');
        var url = $('#remover').data('url');
        var tr = $('tr[data-id="'+idcategoria+'"]');
        $.ajax({
            url: url,
            method: 'post',
            data: {
                id: idcategoria
            },
            dataType: 'json'
        }).done(function(retorno) {

            if(retorno.resultado == 'ok'){
                tr.remove();
            } else if (retorno.motivo == 'invalido'){
                alert('Categoria não existe');
            } else if (retorno.motivo == 'nao-deu'){
                alert('Falha ao deletar');
            }

        });
    });

});