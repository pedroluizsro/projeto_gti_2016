/**
 * Created by pedro on 09/06/16.
 */
$(document).ready( function () {
    
    $('#criar-setor').on('click',function () {
        var botao = $(this);
        var nome = $('#form-setor input[name=nome]').val();
        $.ajax({
            url: botao.data('url'),
            method: 'post',
            data: {
                setor: nome
            },
            dataType: 'json'
        }).done(function(retorno) {

            if(retorno.resultado == 'ok'){
                var tr = '';
                tr += '<td>'+retorno.dados.id+'</td>';
                tr += '<td>'+retorno.dados.setor+'</td>';
                tr += '<td><button type="button" class="remover-setor" data-id="'+retorno.dados.id+'">X</button></td>';
                $('#setores').append('<tr data-id="'+retorno.dados.id+'">'+tr+'</tr>');
                $('#form-setor input[name=nome]').val('');
            } else if (retorno.motivo == 'nao-preenchido') {
                alert('Preencha os campos corretamente');
            } else if (retorno.motivo == 'nao-salvou'){
                alert('Falha ao criar');
            }

        });
    });

    $('#setores').on('click','.remover-setor',function () {
        var idsetor = $(this).data('id');
        var url = $('#remover').data('url');
        var tr = $('tr[data-id="'+idsetor+'"]');
        $.ajax({
            url: url,
            method: 'post',
            data: {
                id: idsetor
            },
            dataType: 'json'
        }).done(function(retorno) {

            if(retorno.resultado == 'ok'){
                tr.remove();
            } else if (retorno.motivo == 'invalido'){
                alert('Usuário não existe');
            } else if (retorno.motivo == 'nao-deu'){
                alert('Falha ao deletar');
            }

        });


    });
    
});