<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/',['as' => '/', 'uses' => 'IndexController@getIndex']);

//Atendentes
$app->get('/atendente',['as' => 'atendente', 'uses' => 'AtendenteController@getIndex']);
$app->post('/atendente/criar',['as' => 'criar-atendente','uses' => 'AtendenteController@postAjaxCriarAtendente']);
$app->post('/atendente/deletar',['as' => 'deletar-atendente','uses' => 'AtendenteController@postAjaxDeletarAtendente']);

//Categorias
$app->get('/categoria',['as' => 'categoria', 'uses' => 'CategoriaController@getIndex']);
$app->post('/categoria/criar',['as' => 'criar-categoria','uses' => 'CategoriaController@postAjaxCriarCategoria']);
$app->post('/categoria/deletar',['as' => 'deletar-categoria','uses' => 'CategoriaController@postAjaxDeletarCategoria']);

//Setores
$app->get('/setor',['as' => 'setor', 'uses' => 'SetorController@getIndex']);
$app->post('/setor/criar',['as' => 'criar-setor','uses' => 'SetorController@postAjaxCriarSetor']);
$app->post('/setor/deletar',['as' => 'deletar-setor','uses' => 'SetorController@postAjaxDeletarSetor']);

//Catalogos
$app->get('/catalogo',['as' => 'catalogo', 'uses' => 'CatalogoController@getIndex']);
$app->post('/catalogo/criar',['as' => 'criar-catalogo','uses' => 'CatalogoController@postAjaxCriarCatalogo']);
$app->post('/catalogo/deletar',['as' => 'deletar-catalogo','uses' => 'CatalogoController@postAjaxDeletarCatalogo']);

//Ordem de Serviços
$app->get('/ordem-servico',['as' => 'ordem-servico', 'uses' => 'OrdemServicoController@getIndex']);
$app->get('/ordem-servico/{id}',['as' => 'ordem-servico-pagina', 'uses' => 'OrdemServicoController@getOrdemServico']);
$app->get('/ordem-servico/abrir/1',['as' => 'abrir-ordem-servico-passo-1', 'uses' => 'OrdemServicoController@getAbrirOrdemServico1']);
$app->post('/ordem-servico/abrir/2',['as' => 'abrir-ordem-servico-passo-2', 'uses' => 'OrdemServicoController@postAbrirOrdemServico2']);
$app->post('/ordem-servico/abrir/3',['as' => 'abrir-ordem-servico-passo-3', 'uses' => 'OrdemServicoController@postAbrirOrdemServico3']);
$app->post('/ordem-servico/abrir/4',['as' => 'abrir-ordem-servico-passo-4', 'uses' => 'OrdemServicoController@postAbrirOrdemServico4']);
$app->post('/ordem-servico/abrir/criar',['as' => 'abrir-ordem-servico', 'uses' => 'OrdemServicoController@postAbrirOrdemServico']);
$app->post('/ordem-servico/interagir',['as' => 'interagir', 'uses' => 'OrdemServicoController@postInteracao']);

$app->get('/voltar',['as' => 'voltar', 'uses' => 'IndexController@getVoltar']);