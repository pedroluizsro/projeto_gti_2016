<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 02/06/16
 * Time: 21:20
 */

namespace App\Http\Controllers;


use App\Catalogo;
use App\Categoria;
use App\Tipo;
use Illuminate\Http\Request;

class CatalogoController extends Controller{

    public function getIndex(){

        //Carrega Categorias
        $categorias = Categoria::all();

        //Carrega Catalogos
        $catalogos = Catalogo::all();

        //Carrega Tipos
        $tipos = Tipo::all();

        //Prepara array de retorno de catalogos cadastrados.
        $catalogosRetorno = array();
        if($catalogos){
            foreach ($catalogos as $key => $catalogo) {
                $catalogosRetorno[$key]['id'] = $catalogo->id;
                $catalogosRetorno[$key]['catalogo'] = $catalogo->catalogo;
                //Busca nome da respectiva categoria.
                foreach ($categorias as $categoria){
                    if($categoria->id == $catalogo->categoria){
                        $catalogosRetorno[$key]['categoria'] = $categoria->categoria;
                    }
                }
                //Busca nome do tipo.
                foreach ($tipos as $tipo){
                    if($tipo->id == $catalogo->tipo){
                        $catalogosRetorno[$key]['tipo'] = $tipo->tipo;
                    }
                }
            }
        }


        $retorno = array(
            'catalogos' => $catalogosRetorno,
            'categorias' => $categorias,
            'tipos' => $tipos,
            'view' => 'catalogo'
        );

        return view('catalogo',$retorno);

    }

    public function postAjaxCriarCatalogo(Request $request){

        $dados = $request->all();

        if(!$dados OR !is_array($dados) OR !$dados['catalogo'] OR !$dados['idcategoria'] OR !$dados['idtipo']){
            $retorno = array(
                'resultado' => 'no',
                'motivo' => 'nao-preenchido'
            );

            return json_encode($retorno);
        }

        $catalogo = new Catalogo();
        $catalogo->catalogo = $dados['catalogo'];
        $catalogo->categoria = (int)$dados['idcategoria'];
        $catalogo->tipo = (int)$dados['idtipo'];

        if(!$catalogo->save()){
            $retorno = array(
                'resultado' => 'no',
                'motivo' => 'nao-salvou'
            );

            return json_encode($retorno);
        }

        //Busca categoria selecionada
        $categoria = Categoria::find((int)$dados['idcategoria']);

        //Busca tipo selecionado
        $tipo = Tipo::find((int)$dados['idtipo']);

        $retorno = array(
            'resultado' => 'ok',
            'dados' => array(
                'id' => $catalogo->id,
                'catalogo' => $catalogo->catalogo,
                'categoria' => $categoria->categoria,
                'tipo' => $tipo->tipo,
            )
        );

        return json_encode($retorno);

    }

    public function postAjaxDeletarCatalogo(Request $request){

        $dados = $request->all();

        if(!$dados OR !is_array($dados) OR !$dados['id']){
            $retorno = array(
                'resultado' => 'no',
                'motivo' => 'invalido'
            );

            return json_encode($retorno);
        }

        $catalogo = Catalogo::find($dados['id']);
        if(!$catalogo->delete()){
            $retorno = array(
                'resultado' => 'no',
                'motivo' => 'nao-deu'
            );

            return json_encode($retorno);
        }

        $retorno = array(
            'resultado' => 'ok',
        );

        return json_encode($retorno);

    }
}