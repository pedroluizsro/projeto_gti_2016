<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 23/06/16
 * Time: 20:00
 */

namespace App\Http\Controllers;


use App\Atendente;
use App\Catalogo;
use App\Categoria;
use App\InteracaoOrdemServico;
use App\OrdemServico;
use App\Setor;
use App\Tipo;
use Illuminate\Http\Request;

class OrdemServicoController {

    public function getIndex(){

        $ordem_servico = OrdemServico::all();

        $retorno = array();
        foreach ($ordem_servico as $ordemservico) {

            $interacoes = null;

            $retorno['ordem_servico'][$ordemservico->id] = $ordemservico;

            //Busca catalogo referente à ordem.
            $catalogo = Catalogo::find($ordemservico->catalogo);
            $retorno['ordem_servico'][$ordemservico->id]['catalogo'] = $catalogo->catalogo;

            //Busca categoria da ordem de serviço.
            $categoria = Categoria::find($catalogo->categoria);
            $retorno['ordem_servico'][$ordemservico->id]['categoria'] = $categoria->categoria;

            $interacoes = InteracaoOrdemServico::whereRaw(
                'ordem_servico = ?',
                array($ordemservico->id)
            )->get();
            if($interacoes and isset($interacoes[0])){
                $atendente = false;
                foreach ($interacoes as $interacao) {
                    $atendente = Atendente::find($interacao->atendente);
                }
                if($atendente){
                    $retorno['ordem_servico'][$ordemservico->id]['atendente'] = $atendente->nome;
                } else {
                    $retorno['ordem_servico'][$ordemservico->id]['atendente'] = false;
                }
                $retorno['ordem_servico'][$ordemservico->id]['status'] = "Em andamento";
            } else {
                $retorno['ordem_servico'][$ordemservico->id]['status'] = "Em aberto";
            }

        }

        if(!$retorno OR empty($retorno)){
            $retorno['ordem_servico'] = false;
        }

        return view('lisgagem_ordem_servico',$retorno);
    }

    public function getOrdemServico($id){

        $retorno['ordem_servico'] = OrdemServico::find($id);

        //Busca catalogo referente à ordem.
        $catalogo = Catalogo::find($retorno['ordem_servico']->catalogo);
        $retorno['ordem_servico']['catalogo'] = $catalogo->catalogo;

        //Busca categoria da ordem de serviço.
        $categoria = Categoria::find($catalogo->categoria);
        $retorno['ordem_servico']['categoria'] = $categoria->categoria;

        $setor = Setor::find($retorno['ordem_servico']->setor);
        $retorno['ordem_servico']['setor'] = $setor->setor;

        $retorno['atendentes'] = Atendente::all();

        $retorno['interacoes'] = InteracaoOrdemServico::whereRaw(
            'ordem_servico = ?',
            array($id)
        )->get();
        
        if($retorno['interacoes']){
            $retorno['status'] = "Em andamento";
        } else {
            $retorno['status'] = "Em aberto";
        }

        return view('ordem_servico',$retorno);

    }

    public function getAbrirOrdemServico1(){

        $retorno = [
            'setores' => Setor::all()
        ];

        return view('abrir-ordem-servico/passo1',$retorno);

    }

    public function postAbrirOrdemServico2(Request $request){

        $dados = $request->all();

        $retorno = array(
            'tipos' => Tipo::all(),
            'setor' => $dados['setor']
        );

        return view('abrir-ordem-servico/passo2',$retorno);

    }

    public function postAbrirOrdemServico3(Request $request){

        $dados = $request->all();

        $retorno = array(
            'categorias' => Categoria::all(),
            'tipo' => $dados['tipo'],
            'setor' => $dados['setor']
        );

        return view('abrir-ordem-servico/passo3',$retorno);

    }

    public function postAbrirOrdemServico4(Request $request){

        $dados = $request->all();

        $catalogos = Catalogo::whereRaw(
            'categoria = ? AND tipo = ?',
            array($dados['categoria'],$dados['tipo'])
        )->get();

        $retorno = array(
            'categoria' => $dados['categoria'],
            'tipo' => $dados['tipo'],
            'setor' => $dados['setor'],
            'catalogos' => $catalogos
        );

        return view('abrir-ordem-servico/passo4',$retorno);

    }

    public function postAbrirOrdemServico(Request $request){

        $dados = $request->all();

        $catalogo = Catalogo::find($dados['catalogo']);

        $ordem_servico = new OrdemServico();
        $ordem_servico->setor = $dados['setor'];
        $ordem_servico->catalogo = $dados['catalogo'];
        $ordem_servico->titulo = $catalogo->catalogo;
        $ordem_servico->mensagem = $dados['texto'];
        $ordem_servico->save();

        return redirect()->route('ordem-servico');

    }

    public function postInteracao(Request $request){

        $dados = $request->all();

        $interacao = new InteracaoOrdemServico();
        $interacao->ordem_servico = $dados['idordemservico'];
        if($dados['atendente'] != 0){
            $interacao->atendente = $dados['atendente'];
        }
        $interacao->mensagem = $dados['resposta'];
        $interacao->save();
        
        return redirect()->route('ordem-servico-pagina',array('id' => $dados['idordemservico']));

    }

}