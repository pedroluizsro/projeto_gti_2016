<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 09/06/16
 * Time: 10:18
 */

namespace App\Http\Controllers;


use App\Atendente;
use Illuminate\Http\Request;

class AtendenteController extends Controller{

    public function getIndex(){

        $atendentes = Atendente::all();

        $retorno = array(
            'atendentes' => $atendentes,
            'view' => 'atendente'
        );

        return view('atendente',$retorno);

    }

    public function postAjaxCriarAtendente(Request $request){

        $dados = $request->all();

        if(!$dados OR !is_array($dados) OR !$dados['nome']){
            $retorno = array(
                'resultado' => 'no',
                'motivo' => 'nao-preenchido'
            );
            
            return json_encode($retorno);
        }

        $atendente = new Atendente();
        $atendente->nome = $dados['nome'];
        
        if(!$atendente->save()){
            $retorno = array(
                'resultado' => 'no',
                'motivo' => 'nao-salvou'
            );
            
            return json_encode($retorno);
        }

        $retorno = array(
            'resultado' => 'ok',
            'dados' => array(
                'id' => $atendente->id,
                'nome' => $atendente->nome
            )
        );

        return json_encode($retorno);

    }

    public function postAjaxDeletarAtendente(Request $request){

        $dados = $request->all();

        if(!$dados OR !is_array($dados) OR !$dados['id']){
            $retorno = array(
                'resultado' => 'no',
                'motivo' => 'invalido'
            );

            return json_encode($retorno);
        }

        $atendente = Atendente::find($dados['id']);
        if(!$atendente->delete()){
            $retorno = array(
                'resultado' => 'no',
                'motivo' => 'nao-deu'
            );

            return json_encode($retorno);
        }

        $retorno = array(
            'resultado' => 'ok',
        );

        return json_encode($retorno);

    }

}