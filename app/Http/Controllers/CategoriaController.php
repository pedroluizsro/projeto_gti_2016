<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 02/06/16
 * Time: 21:20
 */

namespace App\Http\Controllers;

use App\Categoria;
use Illuminate\Http\Request;


class CategoriaController extends Controller{

    public function getIndex(){

        $categorias = Categoria::all();

        $retorno = array(
            'categorias' => $categorias,
            'view' => 'categoria'
        );

        return view('categoria',$retorno);

    }

    public function postAjaxCriarCategoria(Request $request){

        $dados = $request->all();

        if(!$dados OR !is_array($dados) OR !$dados['categoria']){
            $retorno = array(
                'resultado' => 'no',
                'motivo' => 'nao-preenchido'
            );

            return json_encode($retorno);
        }

        $categoria = new Categoria();
        $categoria->categoria = $dados['categoria'];

        if(!$categoria->save()){
            $retorno = array(
                'resultado' => 'no',
                'motivo' => 'nao-salvou'
            );

            return json_encode($retorno);
        }

        $retorno = array(
            'resultado' => 'ok',
            'dados' => array(
                'id' => $categoria->id,
                'categoria' => $categoria->categoria
            )
        );

        return json_encode($retorno);
    }

    public function postAjaxDeletarCategoria(Request $request){

        $dados = $request->all();

        if(!$dados OR !is_array($dados) OR !$dados['id']){
            $retorno = array(
                'resultado' => 'no',
                'motivo' => 'invalido'
            );

            return json_encode($retorno);
        }

        $categoria = Categoria::find($dados['id']);
        if(!$categoria->delete()){
            $retorno = array(
                'resultado' => 'no',
                'motivo' => 'nao-deu'
            );

            return json_encode($retorno);
        }

        $retorno = array(
            'resultado' => 'ok',
        );

        return json_encode($retorno);

    }

}