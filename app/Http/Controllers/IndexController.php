<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 31/05/16
 * Time: 13:54
 */

namespace App\Http\Controllers;


use Laravel\Lumen\Http\Redirector;

class IndexController extends Controller {

    public function getIndex(){
        return view('index');
    }

    public function getVoltar(){

        return redirect()->back();

    }
    
}