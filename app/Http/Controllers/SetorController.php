<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 02/06/16
 * Time: 20:19
 */

namespace App\Http\Controllers;

use App\Setor;
use Illuminate\Http\Request;

class SetorController extends Controller {

    public function getIndex(){

        $setores = Setor::all();

        $retorno = array(
            'setores' => $setores,
            'view' => 'setor'
        );

        return view('setor',$retorno);

    }

    public function postAjaxCriarSetor(Request $request){

        $dados = $request->all();

        if(!$dados OR !is_array($dados) OR !$dados['setor']){
            $retorno = array(
                'resultado' => 'no',
                'motivo' => 'nao-preenchido'
            );

            return json_encode($retorno);
        }

        $setor = new Setor();
        $setor->setor = $dados['setor'];

        if(!$setor->save()){
            $retorno = array(
                'resultado' => 'no',
                'motivo' => 'nao-salvou'
            );

            return json_encode($retorno);
        }

        $retorno = array(
            'resultado' => 'ok',
            'dados' => array(
                'id' => $setor->id,
                'setor' => $setor->setor
            )
        );

        return json_encode($retorno);
    }

    public function postAjaxDeletarSetor(Request $request){

        $dados = $request->all();

        if(!$dados OR !is_array($dados) OR !$dados['id']){
            $retorno = array(
                'resultado' => 'no',
                'motivo' => 'invalido'
            );

            return json_encode($retorno);
        }

        $setor = Setor::find($dados['id']);
        if(!$setor->delete()){
            $retorno = array(
                'resultado' => 'no',
                'motivo' => 'nao-deu'
            );

            return json_encode($retorno);
        }

        $retorno = array(
            'resultado' => 'ok',
        );

        return json_encode($retorno);

    }

}