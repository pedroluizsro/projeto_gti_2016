<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class InteracaoOrdemServico extends Model {

    protected $table = 'interacao_ordem_servico';

    public $timestamps = false;

}