<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class OrdemServico extends Model {

    protected $table = 'ordem_servico';

    public $timestamps = false;

}