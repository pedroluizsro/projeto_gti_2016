<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Catalogo extends Model {

    protected $table = 'catalogo';

    public $timestamps = false;

}