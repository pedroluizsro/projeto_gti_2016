#!/usr/bin/env bash
#Baixa e instala pacotes
wget https://download.postgresql.org/pub/repos/yum/9.5/redhat/rhel-7-x86_64/pgdg-centos95-9.5-2.noarch.rpm
rpm -i pgdg-centos95-9.5-2.noarch.rpm
yum -y update
yum -y install postgresql95-server.x86_64 postgresql95-devel.x86_64 postgresql95-contrib.x86_64 postgresql95-libs.x86_64

#Inicia servi�o
/usr/pgsql-9.5/bin/postgresql95-setup initdb
systemctl enable postgresql-9.5.service
systemctl start postgresql-9.5.service

#Altera arquivos de configura��o.
sed -i -e "s/peer/trust/" /var/lib/pgsql/9.5/data/pg_hba.conf
sed -i -e "s/ident/trust/" /var/lib/pgsql/9.5/data/pg_hba.conf
sed -i -e "s/127\.0\.0\.1\/32/0\.0\.0\.0\/0/" /var/lib/pgsql/9.5/data/pg_hba.conf
sed -i -e "s/#listen_addresses = 'localhost'/listen_addresses = '*'/" /var/lib/pgsql/9.5/data/postgresql.conf
sed -i -e "s/#port = 5432/port = 5432/" /var/lib/pgsql/9.5/data/postgresql.conf

#define senha de usu�rio.
su postgres -c "psql -c \"ALTER USER postgres WITH PASSWORD 'qwe123'\""