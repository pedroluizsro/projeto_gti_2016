#!/usr/bin/env bash

yum -y install php70w.x86_64 php70w-cli.x86_64 php70w-common.x86_64 php70w-devel.x86_64 php70w-mbstring.x86_64 php70w-mcrypt.x86_64 php70w-pdo.x86_64 php70w-pdo_dblib.x86_64 php70w-pecl-xdebug.x86_64 php70w-pgsql.x86_64